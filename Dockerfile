FROM ubuntu:bionic

ADD requirements.txt /tmp/requirements.txt
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    apt-get install -y python3 python3-pip maas-cli curl git bash ca-certificates zip tar openssh-client && \
    pip3 install -r /tmp/requirements.txt

ADD maas-setup.py /usr/local/bin/maas-setup
RUN chmod +x /usr/local/bin/maas-setup && \
    echo 'unset cd' | tee -a ~/.profile

# fix for click in python3
ENV LC_ALL=C.UTF-8 \
    LANG=C.UTF-8
