#!/usr/bin/env python3

import click
import logging
from time import sleep
from maas.client import connect
import os
import subprocess as sp
import string
import random
from datetime import datetime


logging.basicConfig(level=logging.DEBUG if os.getenv('DEBUG') else logging.INFO,
                    format='%(asctime)-15s %(message)s')
log = logging.getLogger(__name__)


@click.group()
def cli():
    pass


def get_machines(api_url, api_key, available_only=False, tags=[]):
    log.debug('{} - {} - {}'.format(api_url, api_key, tags))
    log.info('Connecting to MaaS API at {}.'.format(api_url))
    c = connect(api_url, apikey=api_key)
    log.debug('Connected to MaaS API at {}.'.format(api_url))
    ret_val = c.machines.list()
    if available_only:
        ret_val = [x for x in ret_val if not x.owner]
    if tags:
        ret_val = [x for x in ret_val if set(x.tags).intersection(set(tags))]
    return ret_val


def remove_tags(api_url, api_key, tags=[]):
    tagged_machines = get_machines(api_url, api_key, tags=tags)
    log.info('Found {} machines tagged with one of {}'.format(len(tagged_machines), tags))
    hosts_affected = []
    for x in tagged_machines:
        removed_tags = []
        for t in list(set(x.tags).intersection(set(tags))):
            log.info('Removing tag {} from {}.'.format(t, x.hostname))
            removed_tags.append(t)
            tag_operation('remove', api_url, api_key, x.system_id, t)
        x.save()
        log.info('Saved {} after removing tags: {}.'.format(len(x.hostname), removed_tags))
        hosts_affected.append(x.hostname)
    return hosts_affected


def tag_machines(api_url, api_key, master_tag, node_tag, master_count=1, node_count=2):
    '''This is a dev setup, so we're prioritizing low-end resouces here.
       Node order is relative to (cpu cores) * (amount of ram) in asc order.
       Then we grab resources in order, masters first, then nodes.
    '''
    machines = get_machines(api_url, api_key, available_only=True)
    sorted_list = sorted(machines, key=lambda x: x.cpus*x.memory)
    masters = sorted_list[0:master_count]
    nodes = sorted_list[master_count:node_count+master_count]
    hosts_affected = {'masters': [], 'nodes': []}
    for n, t, l in [('masters', master_tag, masters), ('nodes', node_tag, nodes)]:
        for m in l:
            m.tags.append(t)
            tag_operation('add', api_url, api_key, m.system_id, t)
            m.save()
            hosts_affected[n].append(m.hostname)
    return hosts_affected


def set_state_ready(api_url, api_key, tags=[], sleep_time=1):
    machine_list = get_machines(api_url, api_key, tags=tags)
    not_ready = [x for x in machine_list if x.status_name.lower() != 'ready']
    log.info('{} hosts are not ready yet.'.format(len(not_ready)))
    ret_val = {}
    released = []
    for m in machine_list:
        if m.status_name.lower() == 'ready' or 'err' in m.status_name.lower():
            ret_val[m.hostname] = m.status_name
            log.info('Host {} is already in state {}.'.format(m.hostname, m.status_name))
        else:
            m.release()
            released.append(m)
            log.info('Host {} has been released.'.format(m.hostname))
    while len(released) > 0:
        for m in released:
            m.refresh()
            if m.status_name.lower() == 'ready' or 'err' in m.status_name.lower():
                released.remove(m)
                ret_val[m.hostname] = m.status_name
                log.info('Host {} is now in the Ready state.'.format(m.hostname))
        sleep(sleep_time)
    return ret_val


def tag_operation(op_type, api_url, api_key, system_id, tag):
    '''Since MaaS' api isn't 'tag friendly' yet... we have to use subprocess to
       use the CLI... quick note from docs is here:
            https://docs.maas.io/2.5/en/nodes-tags#tag-management
    '''
    start = datetime.utcnow()
    cmd = []
    maas_profile = ''.join(random.choice(string.ascii_lowercase) for _ in range(4))
    cmd.append('/usr/bin/maas login {} {} {}'.format(maas_profile, api_url, api_key))
    cmd.append('/usr/bin/maas {} tag update-nodes {} {}={}'.format(maas_profile, tag, op_type, system_id))
    cmd_str = ' ; '.join(cmd)
    log.info('Starting run_command for: {}'.format(cmd_str))
    p = sp.Popen(cmd_str, shell=True, bufsize=0, stdout=sp.PIPE, stderr=sp.STDOUT)
    stdout = []
    ret_val = None
    while True:
        line = p.stdout.readline()
        ret_val = p.poll()
        if not line and ret_val != None:
            break
        log.info(line)
    log.info('Completed run_command in {} for: {}'.format(
        (datetime.utcnow() - start).total_seconds(), cmd_str))
    return ret_val


@cli.command()
@click.option('-u', '--maas_url', envvar='MAAS_URL')
@click.option('-k', '--api_key', envvar='API_KEY')
@click.option('-n', '--node_count', envvar='NODE_COUNT', type=int, default=2)
@click.option('-m', '--master_count', envvar='MASTER_COUNT', type=int, default=1)
@click.option('--master_tag', envvar='MASTER_TAG', default='k8s-master')
@click.option('--node_tag', envvar='NODE_TAG', default='k8s-node')
@click.option('--sleep_time', envvar='SLEEP_TIME', default=1)
def prepare(maas_url, api_key, node_count=2, master_count=1, master_tag='k8s-master', node_tag='k8s-node', sleep_time=1):
    results = {}
    results['remove_tags'] = remove_tags(maas_url, api_key, [master_tag, node_tag])
    results['tag_machines'] = tag_machines(maas_url, api_key, master_tag, node_tag, master_count, node_count)
    results['set_state_ready'] = set_state_ready(maas_url, api_key, [master_tag, node_tag], sleep_time)
    log.info('Prepare Complete')
    for k, v in results.items():
        log.info('{} - {}'.format(k, v))


@cli.command()
@click.option('-u', '--maas_url', envvar='MAAS_URL')
@click.option('-k', '--api_key', envvar='API_KEY')
@click.option('--master_tag', envvar='MASTER_TAG', default='k8s-master')
@click.option('--node_tag', envvar='NODE_KEY', default='k8s-node')
@click.option('--sleep_time', envvar='SLEEP_TIME', default=1)
def cleanup(maas_url, api_key, master_tag='k8s-master', node_tag='k8s-node', sleep_time=1):
    results = {}
    results['set_state_ready'] = set_state_ready(maas_url, api_key, [master_tag, node_tag], sleep_time)
    results['remove_tags'] = remove_tags(maas_url, api_key, [master_tag, node_tag])
    log.info('Cleanup Complete')
    for k, v in results.items():
        log.info('{} - {}'.format(k, v))


if __name__ == '__main__':
    cli()
